# e3-base

This repository contains e3-base, an ESS-specific version of EPICS base.

Generally speaking, EPICS base comprises the following functionalities (sub-systems):

    1.) Build system and tools
    2.) Common and OS-interface libraries
    3.) Static and run-time database access routines and database processing
    4.) Channel access client and server libraries
    5.) Device and driver support
    6.) Standard EPICS database records

Note that ESS' Site-specific changes are contained in patch-files, which should be applied prior to building.

**This version contains the patch to compile in the WSL Platform**

# Configuration files

There are naming differences between e3-base's configuration files and other e3 module's configuration files. For e3-base, the main configuration file is named `CONFIG_BASE`.

Open that file and change the path 

`E3_EPICS_PATH:=/epics`

if you want to install to EPICS to another place. 

# Installation

    $ make init
    $ make patch
    $ make build

Above steps will build EPICS base for use with an e3 environment. For more instructions on how to use e3, please visit http://e3.pages.esss.lu.se.
